from nuseq.features.extractors.bam import BamFeatureExtractor
from nuseq.features.extractors.fasta import FastaFeatureExtractor

import pandas as pd
import pysam


def main(args):
    pos_df = pd.read_csv(args.pos_file, sep='\t')

    bam_df = load_bam_features(args.bam_file, pos_df)

    ref_df = load_ref_genome_features(args.ref_genome_fasta_file, pos_df)

    features_df = pd.merge(bam_df, ref_df, on=['chrom', 'coord', 'ref', 'alt'])

    features_df = features_df.set_index(['chrom', 'coord', 'ref', 'alt'])

    features_df.to_csv(args.out_file, sep='\t')


def load_bam_features(bam_file, pos_df):
    df = []

    bam = pysam.AlignmentFile(bam_file)

    extractor = BamFeatureExtractor(bam)

    for _, pos_row in pos_df.iterrows():
        row = extractor.extract_features_from_position(pos_row['chrom'], pos_row['coord'])

        for col in row:
            if col.startswith(pos_row['ref']):
                new_col = 'ref_{}'.format('_'.join(col.split('_')[1:]))

                row[new_col] = row[col]

            if col.startswith(pos_row['alt']):
                new_col = 'alt_{}'.format('_'.join(col.split('_')[1:]))

                row[new_col] = row[col]

        row['ref'] = pos_row['ref']

        row['alt'] = pos_row['alt']

        df.append(row)

    df = pd.concat(df)

    cols = [x for x in df.columns if x[0] not in ('A', 'C', 'G', 'T')]

    return df[cols]


def load_ref_genome_features(ref_genome_fasta_file, pos_df):
    df = []

    ref_genome_fasta = pysam.FastaFile(ref_genome_fasta_file)

    ref_genome_extractor = FastaFeatureExtractor(ref_genome_fasta)

    for _, pos_row in pos_df.iterrows():
        row = ref_genome_extractor.extract_features_from_position(pos_row['chrom'], pos_row['coord'])

        row['ref'] = pos_row['ref']

        row['alt'] = pos_row['alt']

        df.append(row)

    df = pd.concat(df)

    df = df.drop('ref_base', axis=1)

    return df

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-b', '--bam_file', required=True,
        help='''Path of BAM file to extract features from.'''
    )

    parser.add_argument(
        '-p', '--pos_file', required=True,
        help='''Path of tab delimited position file with columns `chrom, coord, ref, alt`.'''
    )

    parser.add_argument(
        '-r', '--ref_genome_fasta_file', required=True,
        help='''Path of reference genome in fasta file. Must have a .fai index file alongside.'''
    )

    parser.add_argument(
        '-o', '--out_file', required=True,
        help='''Path where output will be written.'''
    )

    cli_args = parser.parse_args()

    main(cli_args)
