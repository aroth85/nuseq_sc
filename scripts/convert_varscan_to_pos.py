import pandas as pd


def main(args):
    df = pd.read_csv(args.in_file, sep='\t')

    nucleotides = ['A', 'C', 'G', 'T']

    col_map = {
        'Chrom': 'chrom',
        'Position': 'coord',
        'Ref': 'ref',
        'VarAllele': 'alt'
    }

    df = df.rename(columns=col_map)

    df = df[['chrom', 'coord', 'ref', 'alt']]

    df[df['ref'].isin(nucleotides) & df['alt'].isin(nucleotides)]

    df.to_csv(args.out_file, index=False, sep='\t')

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-i', '--in_file', required=True,
        help='''Path of results file from Varscan'''
    )

    parser.add_argument(
        '-o', '--out_file', required=True,
        help='''Path where output will be written in format compatible with extract_features.py script'''
    )

    cli_args = parser.parse_args()

    main(cli_args)
