# Installation
First you will need to install the latest version of nuseq. 
To do so you will need to install some dependencies. 
Start your conda environment and run  

`conda install cython pandas pysam pyvcf pytables pyyaml scikit-learn scipy -c bioconda` 

to install the required dependencies. 

Now checkout the nuseq code base somewhere and change into the directory. 
The code can be installed by running `python setup.py install`.
Now grab the nuseq single cell code from https://bitbucket.org/aroth85/nuseq. 
Checkout the git repository.

# Running

## Running the feature extractor

The script `scripts/extract_features.py` will extract features. 
Before running the script you need to generate a file with the target positions to extract features for.
This files should have the following columns

1. chrom - The chromosome the target position is on
2. coord - The position on the chromosome of the target position
3. ref - The reference base at the target position
4. alt - The alternative/variant base at the target position

To run the script run

`python PATH_TO_EXTRACT_SCRIPT -b PATH_TO_BAM -p PATH_TO_POS_FILE -r PATH_TO_REF -o PATH_TO_OUTPUT`

where 

- `PATH_TO_EXTRACT_SCRIPT` is the path to the extract_features.py script.   
- `PATH_TO_BAM` is the path to the bam file for the sample
- `PATH_TO_POS_FILE` is the positions file described above
- `PATH_TO_OUTPUT` is where the output will be written in tab delimited format

## Converting Varscan output to a positions file

There is a convenience script `scripts/ the feature extractor.py` that will convert the output from varscan to the positions file format.
The script takes to arguments

- `-i` the path of the varscan results file
- `-o` the path where the positions file will be written out

The output of this script can be used with `scripts/extract_features.py`.
